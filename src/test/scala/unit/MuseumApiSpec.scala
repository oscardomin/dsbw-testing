package unit

import org.scalatest.{BeforeAndAfter, FlatSpec}
import org.mockito.Mockito
import museums.{NewObject, MuseumObject, MuseumApi, Repository}
import server.{JSON, HttpStatusCode}

class MuseumApiSpec extends FlatSpec with BeforeAndAfter {

  var repository: Repository = null
  var api: MuseumApi = null

  before {
    repository = Mockito.mock(classOf[Repository])
    api = new MuseumApi(repository)
  }

  "The Museum API" should "get an object" in {
    val id:Long = 5
    val obj = Some(MuseumObject(id, "Rembrandt", "My painting"))
    Mockito.when(repository.find(id)).thenReturn(obj)

    assert(api.getObject(s"/$id").body === obj)
    assert(api.getObject(s"/$id").status === HttpStatusCode.Ok)

  }

  // What happens when we try to find a work that does not exist?

  "The Museum API" should "post an object" in {
    val id:Long = 5
    var obj =  MuseumObject(id, "Rembrandt", "My painting")
    Mockito.when(repository.create("Rembrandt", "My painting")).thenReturn(obj)
    val objAux = JSON.toJSON(NewObject("Rembrandt","My painting"))
    assert(api.createObject(objAux).body.get === repository.create("Rembrandt", "My painting"))
    assert(api.createObject(objAux).status === HttpStatusCode.Created)
  }

  "The Museum API" should "delete an object" in {
    val id:Long = 5
    val obj = Some(MuseumObject(id, "Rembrandt", "My painting"))
    Mockito.when(repository.remove(id)).thenReturn(obj)

    assert(api.deleteObject(JSON.toJSON(obj)).body === obj)
    assert(api.deleteObject(JSON.toJSON(obj)).status === HttpStatusCode.Ok)
  }

  // What happens when we try to delete a work that does not exist?
}
}
