package unit

import org.scalatest._
import museums.Repository


class RepositorySpec extends FlatSpec {
  var id:Long = 0
  val author = "Whatever"
  val title = "Some random title"

  val r = new Repository()

  "A repository" should "return the newly created object" in {

    val obj = r.create(author, title)
    id = obj.id
    assert(obj.author === author)

    assert(obj.name === title)
  }

  // What can possibly go wrong? Is it ok to have 2 identical works?

  it should "retrieve the object by id" in {
    val obj = r.create(author, title)

    assert(r.find(obj.id).orNull === obj)
  }

  // What happens when we try to find a work that does not exist?

  it should "delete objects" in {
    val obj = r.remove(id)

    assert(obj.get.id === id)
  }

  // What happens when we try to delete a work that does not exist?
}
}
