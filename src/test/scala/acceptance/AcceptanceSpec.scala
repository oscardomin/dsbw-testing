package acceptance

import org.scalatest.{BeforeAndAfter, GivenWhenThen, FeatureSpec}
import server.{JSON, Server}
import museums.{NewObject, Repository, MuseumObject}
import java.net.{HttpURLConnection, URL}


trait Http {
  val OK = 200
  val NOT_FOUND = 404
  val CREATED = 201

  val port = 5050;

  def GET(uri: String): HttpURLConnection = {
    new URL(s"http://localhost:$port/$uri").openConnection().asInstanceOf[HttpURLConnection]
  }

  def POST(uri:String, body:Any): HttpURLConnection = {
    val conn = new URL(s"http://localhost:$port/$uri").openConnection().asInstanceOf[HttpURLConnection]
    conn.setDoOutput(true);
    val json:String = JSON.toJSON(body).value
    conn.getOutputStream().write(json.getBytes())
    conn
  }

  def content(connection: HttpURLConnection): String = {
    scala.io.Source.fromInputStream(connection.getInputStream()).getLines().mkString("\n")
  }
}

class AcceptanceSpec extends FeatureSpec with Http with GivenWhenThen with BeforeAndAfter {

  val repository = new Repository()
  val server = new Server(new museums.MuseumApi(repository), port)

  before {
    server.start()
  }

  after {
    server.stop()
  }

  feature("Get an object") {
    scenario("Get an object that exists") {
      Given("an object in the system")
      val obj = repository.create("The authore", "This is the title")

      When("I request the object")
      val connection = GET("" + obj.id)

      Then("I get a 200 status")
      assert(connection.getResponseCode() === OK)
      And("the object")
      assert(content(connection) === JSON.toJSON(obj).value)
    }

    scenario("Get an object that does not exist") {
      When("I request an object that does not exist")
      val connection = GET("" + System.currentTimeMillis())

      Then("I get a 404 status")
      assert(connection.getResponseCode() === NOT_FOUND)
    }
  }

  feature("Post an object") {
    scenario("Create a new object") {
      When("I create a new object")
      val author = "Picasso"
      val title = "Man in a beret"
      val connection = POST("create", NewObject(author, title))

      Then("I get a 201 status")
      assert(connection.getResponseCode() === CREATED)

      And("a Location header with the link to the object")
      val location = connection.getHeaderField("Location")
      assert(location != null)


      When("I follow the location header")
      // You are not following the location header but, instead, using the
      // repository! Therefore, you are not testing the location header.


      Then("I get the object")
      val obj = repository.find(location.substring(1).toLong)
      assert(content(connection) === JSON.toJSON(obj).value)
    }
  }

  feature("Delete an object") {
    scenario("Delete an object that exists") {
      Given("an object in the system")
      val obj = repository.create("The authore", "This is the title")

      When("I delete an existing object")
      // It should be a DELETE request!! Read the wording!
      val connection = POST("delete", obj)

      Then("I get a 200 status")
      assert(connection.getResponseCode() === OK)

      // What for? You are deleting it, right? It no longer exists!
      And("the object")
      assert(content(connection) === JSON.toJSON(obj).value)
    }

    scenario("Delete an object that does not exist") {

      // The When should be When("I request to delete an object that does not exist")
      When("I request an object that does not exist")
      val id:Long = 5000
      // What do you want those vals for??
      val title = ""
      val author = ""
      val obj = MuseumObject(id, title, author)

      // Why? And, please, don't leave println's in code!
      var connection = GET("" + obj.id)
      println(connection.getResponseCode())

      connection = POST("delete", obj)


      // Why? OK would be more natural.
      Then("I get a 404 status")
      assert(connection.getResponseCode() === NOT_FOUND)
    }
  }
}
