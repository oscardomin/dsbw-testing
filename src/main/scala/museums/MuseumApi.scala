package museums

import server.{JSON, Response, HttpStatusCode}


case class NewObject(author:String, title:String)

class MuseumApi(repository:Repository) extends server.Api {

  def service(method: String, uri: String, parameters: Map[String, List[String]] = Map(), headers: Map[String, String] = Map(), body: Option[JSON] = None): Response = {
    (method) match {
      case "GET" => getObject(uri)
      case "POST" =>
        (uri) match {
          case "/create" => createObject(body.get)
          case "/delete" => deleteObject(body.get)
        }
      case _ => Response(HttpStatusCode.Ok, "Hello world!")
    }
  }

  def getObject(uri:String): Response = {
    val id = uri.substring(1).toLong
    repository.find(id) match {
      case Some(x) => Response(HttpStatusCode.Ok, x)
      case None => Response(HttpStatusCode.NotFound, "Not found");
    }
  }

  def createObject(body:JSON): Response = {
    val req = JSON.fromJSON[NewObject](body)
    val obj = repository.create(req.author, req.title)
    Response(HttpStatusCode.Created, obj, Map("Location" -> s"/${obj.id}"))
  }

  def deleteObject(body:JSON): Response = {
    val req = JSON.fromJSON[MuseumObject](body)
      repository.remove(req.id) match {
      case Some(x) => Response(HttpStatusCode.Ok, x)
      case None => Response(HttpStatusCode.NotFound, "Not found");
    }
  }
}
