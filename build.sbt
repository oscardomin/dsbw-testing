resolvers += "scalatools" at "https://oss.sonatype.org/content/groups/scala-tools/"

organization := "edu.upc.fib"

name := "museums"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.10.2"

libraryDependencies ++= Seq(
        "org.scalatest" % "scalatest_2.10" % "2.0" % "test",
	"org.mockito" % "mockito-all" % "1.9.0" % "test",
	"org.eclipse.jetty" % "jetty-servlet" % "8.1.0.RC5",
	"javax.servlet" % "servlet-api" % "2.5",
        "net.liftweb" %% "lift-json" % "2.5"
)

